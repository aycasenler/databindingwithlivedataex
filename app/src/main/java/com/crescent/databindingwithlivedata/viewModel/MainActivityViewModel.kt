package com.crescent.databindingwithlivedata.viewModel

import androidx.databinding.Bindable
import androidx.databinding.Observable
import androidx.databinding.PropertyChangeRegistry
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.crescent.databindingwithlivedata.repository.FakeDataRepository

class MainActivityViewModel : ViewModel() {

    val currentRandomFruitName: LiveData<String>
        get() = FakeDataRepository.currentRandomFruitName

    fun onChangeRandomFruitClick() = FakeDataRepository.changeCurrentRandomFruitName()


    val editTextContent = MutableLiveData<String>()

    private val _displayedEditTextContent = MutableLiveData<String>()
    val displayedEditTextContent: LiveData<String>
        get() = _displayedEditTextContent

    fun onDisplayEditTextContentClick() {
        _displayedEditTextContent.value = editTextContent.value
    }

    fun onSelectRandomEditTextFruit() {
        editTextContent.value = FakeDataRepository.getRandomFruitName()
    }

//    private val callbacks: PropertyChangeRegistry by lazy { PropertyChangeRegistry()}
//
//    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
//        callbacks.add(callback)
//    }
//
//    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
//        callbacks.remove(callback)
//    }
}